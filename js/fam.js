let lastKnownScrollPosition = 0;
let ticking = false;
function triggerClass(scrollPos) {
    let ww = window.outerWidth;

    if(ww > 768 && scrollPos > 200){
        document.body.classList.add('fixed-nav');
    }else{
        document.body.classList.remove('fixed-nav');
    }

}

document.addEventListener('scroll', function(e) {
    lastKnownScrollPosition = window.scrollY;

    if (!ticking) {
        window.requestAnimationFrame(function() {
            triggerClass(lastKnownScrollPosition);
            ticking = false;
        });

        ticking = true;
    }
});